package com.kshrd.srmybatisspringdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Role {

    private  int id;
    private String role_name;
}
