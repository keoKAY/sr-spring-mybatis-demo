package com.kshrd.srmybatisspringdemo.service.serviceImp;

import com.kshrd.srmybatisspringdemo.model.Student;
import com.kshrd.srmybatisspringdemo.repository.StudentRepository;
import com.kshrd.srmybatisspringdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {

    /// inject
    @Autowired
    StudentRepository studentRepository;



    @Override
    public List<Student> getAllStudents() {
        return studentRepository.getAllStudent();
    }

    @Override
    public List<Student> findAllStudentUsingProvider() {
        return studentRepository.findAllStudentUsingProvider();
    }

    @Override
    public List<Student> findStudentByWhereProvider(Student student) {
        return studentRepository.findStudentByWhereProvider(student);
    }

    @Override
    public Student findByID(int id) {
        return studentRepository.findByID(id);
    }

    @Override
    public Boolean updateUser(Student student) {
        return studentRepository.updateStudent(student);
    }

    @Override
    public Boolean insertUser(Student student) {
        return studentRepository.insertUser(student);
    }

    @Override
    public Boolean deleteUser(int id) {
        return studentRepository.deleteStudent(id);
    }
}
