package com.kshrd.srmybatisspringdemo.service;

import com.kshrd.srmybatisspringdemo.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {


    List<Student> getAllStudents();
    List<Student> findAllStudentUsingProvider();
    List<Student> findStudentByWhereProvider(Student student);



    // find by id
    Student findByID(int id );

    //  update
    Boolean updateUser(Student student);

    // insert
    Boolean insertUser(Student student) ;

    // delete

    Boolean deleteUser(int id);


}
