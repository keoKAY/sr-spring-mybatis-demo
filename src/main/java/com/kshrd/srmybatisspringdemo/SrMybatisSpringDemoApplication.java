package com.kshrd.srmybatisspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrMybatisSpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrMybatisSpringDemoApplication.class, args);
    }

}
