package com.kshrd.srmybatisspringdemo.repository;

import com.kshrd.srmybatisspringdemo.model.Course;
import com.kshrd.srmybatisspringdemo.model.Role;
import com.kshrd.srmybatisspringdemo.model.Student;
import com.kshrd.srmybatisspringdemo.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository {

    @SelectProvider(type = UserProvider.class,method = "getAllUser")
    List<Student> findAllStudentUsingProvider();

    @SelectProvider(type = UserProvider.class, method = "selectUserByWhere")
    List<Student> findStudentByWhereProvider(@Param("student") Student student);

    @Select("select * from students")

    @Results(
            {
             @Result(property = "id",column = "id"),
             @Result(property = "username",column = "username"),
             @Result(property = "gender",column = "gender"),
             @Result(property = "bio",column = "bio"),
             @Result(property = "course",column = "course_id",one = @One(select = "findCourseById")),
             @Result(property = "roles",column = "id",many = @Many(select = "findRoleByID"))


            }
    )
    public List<Student>getAllStudent();
    @Select("select * from course where id =#{course_id}")
   Course findCourseById(int course_id);

    @Select("select roles.id, roles.role_name from roles inner join user_role ur on roles.id = ur.role_id where ur.user_id= #{id}")
  List<Role> findRoleByID(int id);





    // find BY id

    @Select("select * from students where id=#{id}")
    public Student findByID(int id);

    // insert
    @Insert("insert into students(id,username,gender, bio,course_id) values (#{student.id},#{student.username},#{student.gender},#{student.bio},#{student.course_id})")
    public Boolean insertUser (@Param("student") Student student);

    // update
    @Update("update students set username=#{student.username},gender=#{student.gender},bio=#{student.bio},course_id=#{student.course_id} where id = #{student.id}")
    public  Boolean updateStudent(@Param("student") Student student);

    // Delete
    @Delete("delete from students where id=#{id}")
    public Boolean deleteStudent(int id);



}
