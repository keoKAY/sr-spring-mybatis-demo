package com.kshrd.srmybatisspringdemo.repository.provider;

import com.kshrd.srmybatisspringdemo.model.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    // get All Student

    public String getAllUser(){

        return new SQL(){{
            SELECT("*");
            FROM("students");
        }}.toString();
    }


    ///
    public String selectUserByWhere(@Param("student") Student student){


        return new SQL(){{
            SELECT("*");
            FROM("students");
            // username
            if (student==null){
                WHERE("1=1"); // true so it's no where
            }else  {
                if (student.getUsername().isEmpty() ){
                    WHERE("id=#{student.id}");
                }else {
                    WHERE("username like #{student.username}");
                }

            }
            // id

            // get all



        }}.toString();
    }




}
